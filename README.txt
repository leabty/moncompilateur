Grammaire du compilateur:

Elements "de base":

	string TYPETOSTRING(TYPE t) //permet de convertir un nombre en string
	void DisplayStatement(void) // DisplayStatement := DISPLAY <Expression> //permet d'afficher des entiers
	TYPE Factor(void)
	OPMUL MultiplicativeOperator(void)
	TYPE Term(void) // Term := Factor {MultiplicativeOperator Factor}
	OPADD AdditiveOperator(void) // AdditiveOperator := "+" | "-" | "||"
	TYPE SimpleExpression(void) // SimpleExpression := Term {AdditiveOperator Term}
	enum TYPE Type(void)
	void VarDeclaration(void) // VarDeclaration := Ident {"," Ident} ":" Type
	void VarDeclarationPart(void) // VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
	OPREL RelationalOperator(void) // RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="
	TYPE Expression(void) // Expression := SimpleExpression [RelationalOperator SimpleExpression]
	void AssignementStatement(void) // AssignementStatement := Identifier ":=" Expression
	void Statement(void) // Statement := AssignementStatement
	void StatementPart(void) // StatementPart := Statement {";" Statement} "."
	void IfStatement(void) //IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
	void WhileStatement(void) //WhileStatement := "WHILE" Expression "DO" Statement
	void BlockStatement(void) //BlockStatement := "BEGIN" Statement { ";" Statement } "END"
	void Program(void) // Program := [VarDeclarationPart] StatementPart


Elements ajoutés:
	TYPE Identifier(void)
		retourne le type de l'ID
	TYPE Number(void)
		retourne le type UNSIGNED_INT
	TYPE Booleen(void)
		retourne le type BOOLEAN
	TYPE Char(void)
		retourne le type CHAR

	void ForStatement(void) // ForStatement := "For" ID ":=" Expression ("TO"|"DOWNTO") Expression "DO" Statement
		ForStatement permet d'effectuer des boucles "for" incrémentales (TO) ou décrémentales (DOWNTO)



