//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"
//HELLO
#include <map>
#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE {UNSIGNED_INT, BOOLEAN, CHAR, DOUBLE};


TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
set<string> Variables;
map <string, TYPE> DeclaredVariables;
unsigned long long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}


	

string TYPETOSTRING(TYPE t)
{
	switch(t)
	{
		case UNSIGNED_INT:
			return "INT";
		break;
		case BOOLEAN:
			return "BOOLEAN";
		break;
	}
} 	
	
		
TYPE Identifier(void)
{
	cout << "\tpush "<<lexer->YYText()<<endl;
	TYPE typeobject=DeclaredVariables[lexer->YYText()];
	current=(TOKEN) lexer->yylex();
	return typeobject;
}

TYPE Number(void)
{
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return UNSIGNED_INT;
}


TYPE Booleen(void)
{
	if(strcmp(lexer->YYText(),"false")==0)
		cout <<"\tpush $0\t\t# False"<<endl;

	if(strcmp(lexer->YYText(),"true")==0)
		cout <<"\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl; //Si on a bien un booléen on empile VRAI

	current=(TOKEN) lexer->yylex();
	return BOOLEAN;
}


TYPE Char(void)
{
	cout <<"\tmovq $0, %rax"<<endl;
	cout <<"\tmovb $'"<<lexer->YYText()<<"' , %al"<<endl;
	cout <<"\tpush %rax"<<endl;

	current=(TOKEN) lexer->yylex();
	if (current!=QUOTEMARK)
		Error(" ' était attendu");		// "'" expected
	return CHAR;
}







TYPE Expression(void);			// Called by Term() and calls Term()
void IfStatement(void);
void WhileStatement(void);
void ForStatement(void);
void BlockStatement(void);

void DisplayStatement(void) // DisplayStatement := DISPLAY <Expression>
{
	int tag = ++TagNumber;
	if (strcmp(lexer->YYText(),"display")!=0)
		Error("display attendu");
	else
	{
		enum TYPE type;
		current=(TOKEN) lexer->yylex();
		cout << "DISPLAY"<<tag<<":"<<endl;
		type=Expression();
		if (type==UNSIGNED_INT)
		{
			cout << "\tpop %rsi\t\t\t# Valeur à afficher"<<endl;
			cout << "\tmovq $FormatString1, %rdi"<<endl;  //\t# \"%llu\\n\"
			cout << "\tmovl	$0, %eax"<<endl;
			cout << "\tcall	printf@PLT"<<endl;

		}
		else if (type==BOOLEAN)
		{
			cout << "\tpop %rdx\t"<<endl;
			cout << "\tcmpq $0, %rdx"<<endl;
			cout << "\tje False"<<tag<<endl;
			cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\""<<endl;
			cout << "\tjmp Next"<<tag<<endl;
			cout << "False"<<tag<<":"<<endl;
			cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\""<<endl;
			cout << "Next"<<tag<<":"<<endl;
			cout << "\tcall	puts@PLT"<<endl;

		}
		else if (type==CHAR)
		{
			cout << "\tpop %rsi	# The value to be displayed"<<endl;
			cout << "\tmovq $FormatString2, %rdi\t# \"%c\\n\""<<endl;
			cout << "\tmovl $0, %eax"<<endl;
			cout << "\tcall	printf@PLT"<<endl;

		}
		else if (type==DOUBLE)
		{
			cout << "\tmovsd	(%rsp), %xmm0\t\t"<<endl;
			cout << "\tsubq	$16, %rsp\t\t"<<endl;
			cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
			cout << "\tmovq $FormatString3, %rdi\t# \"%lf\\n\""<<endl;
			cout << "\tmovq	$1, %rax"<<endl;
			cout << "\tcall	printf"<<endl;
			cout << "nop"<<endl;
			cout << "\taddq $24, %rsp\t\t\t"<<endl;

		}
		else
			Error("Le type n'est ni un entier, ni un char, ni un booléen, ni un double");
	}
}


TYPE Factor(void){
	TYPE type_factor;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type_factor = Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else if (current==NUMBER)
			type_factor=Number();

	else if (current==QUOTEMARK)
	{
		current=(TOKEN) lexer->yylex();
		type=Char();
		current=(TOKEN) lexer->yylex();
	}

	else if(current==ID)
			type_factor=Identifier();
	else if( (strcmp(lexer->YYText(),"true")==0) or (strcmp(lexer->YYText(),"false")==0))
			type=Booleen(); //Si on a un TRUE ou FALSE on a obligatoirement un booléen

	else
		Error("'(' ou chiffre ou lettre attendue");	
	return type_factor;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	TYPE type1,type2;
	OPMUL mulop;
	type1=Factor();
	while(current==MULOP)
	{
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2=Factor();
		if(type1!=type2)
		{
			Error("Les 2 facteurs sont de types différents");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop)
		{
			case AND:
			if(type2!=BOOLEAN)
			{
				Error("il faut un booléen pour l'opérateur AND");
			}
				cout << "\tpop %rbx"<<endl;			// get first operand
				cout << "\tpop %rax"<<endl;	
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(type2==UNSIGNED_INT)
				{
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
				}
				else if(type2==DOUBLE)
				{
						cout<<"\tfldl	8(%rsp)\t"<<endl;
						cout<<"\tfldl	(%rsp)\t\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
						cout<<"\tfmulp	%st(0),%st(1)\t\t\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
						cout<<"\tfstpl 8(%rsp)"<<endl;
						cout<<"\taddq	$8, %rsp\t\t# result on stack's top"<<endl; 
				}
				else
					Error("Il faut un entier ou un double");

				break;
			case DIV:
				if (type2==UNSIGNED_INT)
				{
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result	
				}
				else if (type2==DOUBLE)
				{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t\t\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfdivp	%st(0),%st(1)\t\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t\t# result on stack's top"<<endl;
				}
				else
					Error("Il faut un entier ou un double");
				break;
			case MOD:
				if (type2!=UNSIGNED_INT)
				{
					Error("Il faut un entier");
				}
				cout << "\tpop %rbx"<<endl;			// get first operand
				cout << "\tpop %rax"<<endl;	
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
		
	}
	return type1;
	
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void)
{
	TYPE type1,type2;
	OPADD adop;
	type1=Term();
	while(current==ADDOP)
	{
		adop=AdditiveOperator();		// Save operator in local variable
		type2=Term();
		if(type2!=type1)
		{
			Error("Les 2 facteurs sont de types différents");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				if(type2!=BOOLEAN)
					Error("il faut un booléen pour l'opérateur OR");
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				if (type2==UNSIGNED_INT)
				{
					cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				}
				else if (type2==DOUBLE)
				{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t\t\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)\t\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t\t# result on stack's top"<<endl;
				}
				else
					Error("Entier ou double attendu");
				break;			
			case SUB:	
				if (type2==UNSIGNED_INT)
					cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				else if (type2==DOUBLE)
				{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t\t\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfsubp	%st(0),%st(1)\t\t# %st(0) <- op1 - op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t\t# result on stack's top"<<endl;

				}
				else
					Error("Entier ou double attendu");
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return type1;
}



enum TYPE Type(void)
{
	if(current!=KEYWORD)
		Error("type attendu");
	if(strcmp(lexer->YYText(),"BOOLEAN")==0){
		current=(TOKEN) lexer->yylex();
		return BOOLEAN;
	}	
	else if(strcmp(lexer->YYText(),"UNSIGNED_INT")==0){
		current=(TOKEN) lexer->yylex();
		return UNSIGNED_INT;
	}
	else if(strcmp(lexer->YYText(),"DOUBLE")==0){
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else if(strcmp(lexer->YYText(),"CHAR")==0){
		current=(TOKEN) lexer->yylex();
		return CHAR;


	else
		Error("type inconnu");
	
}




// VarDeclaration := Ident {"," Ident} ":" Type
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclaration(void)
{

	if(current!=ID)
		Error("Un identificateur était attendu");
	
	Variables.insert(lexer->YYText());
	
	current=(TOKEN) lexer->yylex();

	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		
		Variables.insert(lexer->YYText());
		
		current=(TOKEN) lexer->yylex();
	}
	if(strcmp(lexer->YYText(), ":")!=0)
		Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex();
	
	TYPE type;
	if(strcmp(lexer->YYText(), "INTEGER")==0) {
		type=INTEGER;
	}
	else if(strcmp(lexer->YYText(), "BOOLEAN")==0) {
		type=BOOLEAN;
	}
	else if(strcmp(lexer->YYText(), "CHAR")==0) {
		type=CHAR;
	}
	else if(strcmp(lexer->YYText(), "DOUBLE")==0) {
		type=DOUBLE;
	}
	else{
		Error("Type de variable attendu");
	}

	
	for (set<string>::iterator i = Variables.begin(); i != Variables.end(); ++i)
    {
        DeclaredVariables[*i]=type;
        if (type==UNSIGNED_INT)
        {
        	cout << *i << ":\t.quad 0"<<endl;
        }
        else if (type==BOOLEAN)
        {
        	cout << *i << ":\t.quad 0"<<endl;
        }
        else if (type==CHAR)
        {
        	cout << *i << ":\t.byte 0"<<endl;
        }
        else if (type==DOUBLE)
        {
        	cout << *i << ":\t.double 0.0"<<endl;
        }
    }
	Variables.clear();
	current=(TOKEN) lexer->yylex();
}



void VarDeclarationPart(void)
{
	if(strcmp(lexer->YYText(),"VAR")!=0)
		Error("VAR attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	cout << "FormatString1:\t.string \"%llu\\n\"\t# used by printf to display 64-bit unsigned integers"<<endl;
	cout << "FormatString2:\t.string \"%c\"\t\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "FormatString3:\t.string \"%lf\"\t\t# used by printf to display 64-bit floating point numbers"<<endl;
	cout << "TrueString:\t.string \"TRUE\"\t\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\"\t\t# used by printf to display the boolean value FALSE"<<endl; 

	current=(TOKEN) lexer->yylex();
	VarDeclaration();
	
	while(current==SEMICOLON)
	{
		VarDeclaration();
	}
	if(current!=DOT)
		Error("'.' attendu");
	current=(TOKEN) lexer->yylex();

}



// DeclarationPart := "[" Ident {"," Ident} "]"
/*void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificateur était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}*/

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void)
{
	TYPE type1,type2;
	OPREL oprel;
	type1=SimpleExpression();
	if(current==RELOP)
	{
		oprel=RelationalOperator();
		type2=SimpleExpression();
		if(type1!=type2)
		{
			Error("Les deux facteurs sont de types différents");
		}
		if(type1==DOUBLE)
		{
			cout<<"\tfldl	(%rsp)\t"<<endl;
			cout<<"\tfldl	8(%rsp)\t\t\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\t addq $16, %rsp\t\t\t# 2x pop nothing"<<endl;
			cout<<"\tfcomip %st(1)\t\t\t# compare op1 and op2 -> %RFLAGS and pop"<<endl;
			cout<<"\tfaddp %st(1)\t\t\t# pop nothing"<<endl;
		}

		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel)
		{
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOLEAN;
	}
	return type1;
}

// AssignementStatement := Identifier ":=" Expression
string AssignementStatement(void)
{
	TYPE type1,type2;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText()))
	{
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type1=DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type2=Expression();
	if (type1!=type2)
	{
		//Error("La variable est de type " << TYPETOSTRING(type1) << "l'expression est de type " << TYPETOSTRING(type2) );
		Error("La variable et l'expression ont un type différent");
	}
	
	cout << "\tpop "<<variable<<endl;
	return variable;
}

// Statement := AssignementStatement
void Statement(void)
{
	if (current==KEYWORD)
	{
		if(strcmp(lexer->YYText(),"if")==0)
			IfStatement();
		else if(strcmp(lexer->YYText(),"while")==0)
			WhileStatement();
		else if(strcmp(lexer->YYText(),"for")==0)
			ForStatement();
		else if(strcmp(lexer->YYText(),"begin")==0)	
			BlockStatement();
		else if(strcmp(lexer->YYText(),"display")==0)
			DisplayStatement();
		else
			Error("Le mot clé est inconnu");
	}
	else 
	{
		if (current == ID)
			AssignementStatement();
		else
			Error("Une instruction est attendue");
	}
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void)
{
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
	{
		Error("caractère '.' attendu");
	}
	current=(TOKEN) lexer->yylex();
}

//IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void)
{	
	int tag = ++TagNumber;
	if (current!=KEYWORD)
		Error("mot clé attendu");
	else
	{
		if(strcmp(lexer->YYText(),"if")!=0)
		{
			Error(" 'if' attendu");
		}
		else
		{
			cout <<"IF" << tag << ":" <<endl;
			current=(TOKEN) lexer->yylex();
			TYPE type = Expression();
			if(type!=BOOLEAN)
			{
				Error("l'expression doit être booléenne");
			}
			cout << "\tpop %rax"<<endl;
			cout << "\tcmpq $0 ,%rax"<<endl;
			cout << "\tje ELSE" << tag <<endl;
			if(strcmp(lexer->YYText(),"then")!=0)
			{
				Error(" 'then' attendu");
			}
			else
			{
				cout << "THEN" << tag << ":" <<endl;
				current=(TOKEN) lexer->yylex();
				Statement();
				cout << "\tjmp FinSi" << tag <<endl;
				cout <<"ELSE" << tag << ":" <<endl;
				if(strcmp(lexer->YYText(),"else")==0)
				{
					current=(TOKEN) lexer->yylex();
					Statement();		
				}
				cout <<"FinSi" << tag << ":" <<endl;
			}
		}
	}
}

//WhileStatement := "WHILE" Expression "DO" Statement
void WhileStatement(void)
{
	int tag = ++TagNumber;
	if (current!=KEYWORD)
	{
		Error("mot clé attendu");
	}
	else
	{
		if(strcmp(lexer->YYText(),"while")!=0)
		{
			Error("'while' attendu");
		}
		else
		{
			cout <<"WHILE" << tag << ":" <<endl;
			current=(TOKEN) lexer->yylex();
			TYPE type=Expression();
			if(type!=BOOLEAN)
			{
				Error("l'expression doit être booléenne");
			}
			cout<<"\tpop %rax\t# Get the result of expression"<<endl;
			cout<<"\tcmpq $0, %rax"<<endl;
			cout<<"\tje FinWhile"<<tag<<endl;
			
			if(strcmp(lexer->YYText(),"do")!=0)
			{
				Error("'do' attendu");
			}
			else
			{
				current=(TOKEN) lexer->yylex();
				Statement();
				cout<<"\tjmp WHILE"<<tag<<endl;
			}
			cout<<"FinWhile"<<tag<<":"<<endl;
		}
	}
}



// ForStatement := "For" ID ":=" Expression ("TO"|"DOWNTO") Expression "DO" Statement
void ForStatement(void)
{
	int tag = ++TagNumber;
	enum TYPE type;
	if(current!=KEYWORD)
	{
		Error("mot clé attendu");
	}
	else
	{
		if(strcmp(lexer->YYText(), "for")!=0)
			Error(" 'for' attendu");
		else
		{
			current=(TOKEN) lexer->yylex();						
			string variable = lexer->YYText();
			AssignementStatement();			
			cout << "DebutFor"<<tag<<":"<<endl;                   					
			
	        if(strcmp(lexer->YYText(), "to")=0)
	        {
	        	cout << "TO"<<tag<<":"<<endl;
				current=(TOKEN) lexer->yylex();
				val=current;
				Expression();
				
				cout << "ToDo"<<tag<<":"<<endl;					
				
				if(strcmp(lexer->YYText(), "do")==0)
				{
					current=(TOKEN) lexer->yylex();
					cout << "\tpop %rbx" <<endl;
					cout << "\tmovq "<<variable<<", %rax"<<endl;
					cout << "\tcmpq %rbx, %rax"<<endl;
					cout << "\tja FinFor"<<tag<<endl;
					cout << "\tpush %rbx" <<endl;
					Statement();
					cout << "\tmovq "<<variable<<", %rax"<<endl;
					cout << "\taddq $1, %rax"<<endl;
					cout << "\tmovq %rax, "<<variable<<endl;
					cout << "\tjmp ToDo"<<tag<<endl;

				
				}
				else
					Error("do attendu");
			}
			
	        else if(strcmp(lexer->YYText(), "downto")==0)
	        {
				cout << "DOWNTO"<<tag<<":"<<endl;
				current=(TOKEN) lexer->yylex();
				val=current;
				Expression();
							
				cout << "DowntoDO"<<tag<<":"<<endl;
				if(strcmp(lexer->YYText(), "do")==0)
				{
					current=(TOKEN) lexer->yylex();
					cout << "\tpop %rbx" <<endl;
					cout << "\tmovq "<<variable<<", %rax"<<endl;
					cout << "\tcmpq %rbx, %rax"<<endl;
					cout << "\tjb FinFor"<<tag<<endl;
					cout << "\tpush %rbx" <<endl;
					Statement();
					cout << "\tmovq "<<variable<<", %rax"<<endl;
					cout << "\tsubq $1, %rax"<<endl;
					cout << "\tmovq %rax, "<<variable<<endl;
					cout << "\tjmp DowntoDO"<<tag<<endl;
				}
				else
				{
				Error("'do' attendu");
				}
			}
			else
				Error("to ou downto attendu");
		}
	}
	cout << "FinFor"<<tag<<":"<<endl;	
}

//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void)
{
	int tag = ++TagNumber;
	if (current!=KEYWORD)
	{
		Error("mot clé attendu");
	}
	else
	{
		if(strcmp(lexer->YYText(),"debut")!=0)
		{
			Error("'begin' attendu");
		}
		else
		{
			current=(TOKEN) lexer->yylex();
			cout <<"DEBUT" << tag << ":" <<endl;
			
			Statement();
			while(current==SEMICOLON)
			{
				current=(TOKEN) lexer->yylex();
				Statement();
			}
			if(strcmp(lexer->YYText(),"fin")!=0)
			{
				Error("'end' attendu");
			}
			else
			{
				current=(TOKEN) lexer->yylex();
				cout << "FIN"<<tag<<":"<<endl;
				cout << "\tjmp FinBlock" << tag << endl;
			}
			
		}
	}
	cout <<"FinBlock" << tag << ":" <<endl;
}



// Program := [VarDeclarationPart] StatementPart
void Program(void){
	if(current==KEYWORD && strcmp(lexer->YYText(),"VAR")==0)
		VarDeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << ".data"<<endl;
	cout << "FormatString1:\t.string \"%llu\\n\"\t# used by printf to display 64-bit unsigned integers"<<endl;

	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}